Prototype to test C++ REST SDK - codename Casablanca (Microsoft)
------------------------------------------------------

Basic C++ project with CMake, Qt5, Boost and C++ REST SDK (Microsoft API).

Author: [Juliana Hohara](http://juhohara.gitlab.io/)

Prototype:

- Purpose: to test C++ REST SDK, codename Casablanca (Microsoft);
- [Using the principles of the Cmake Reflection Template project](http://onqtam.com/programming/2017-09-02-simple-cpp-reflection-with-cmake/)
	- Python script: this project doesn't use std::any<> from "any" as in the original, instead it uses json::value from the C++ REST SDK (Microsoft);
- [Inspired by sample source code for cpprestsdk (casablanca)](https://github.com/Pintulalm/Restweb)
- Important: install Python (cmake will run the script to Cmake Reflection Template)
- Client application (Spring MVC) [that will access this server](https://gitlab.com/juhohara/prototype-cpprestclient).
- Page: [http://127.0.0.1:34568](http://127.0.0.1:34568)
