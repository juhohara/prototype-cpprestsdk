﻿#ifndef HANDLER_H
#define HANDLER_H

#include <cpprest/http_client.h>
#include <cpprest/http_listener.h>

using namespace std;
using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;
using namespace concurrency;
using namespace streams;

class Handler
{
  public:

    Handler();

    Handler(utility::string_t url);

    virtual ~Handler();

    pplx::task<void>open(){
      return m_listener.open();
    }

    pplx::task<void>close(){
      return m_listener.close();
    }
      
  private:

    void handle_get(http_request message);

    void handle_put(http_request message);

    void handle_post(http_request message);

    void handle_delete(http_request message);

    void handle_error(pplx::task<void>& t);

    void handle_options(http_request request);
    
    http_listener   m_listener;
};

#endif // HANDLER_H

