﻿#include "Handler.h"
#include "Person.h" 

// Qt
#include <QFile>
#include <QTextStream>
#include <QString>

Handler::Handler()
{
   
}

Handler::Handler(utility::string_t url):
    m_listener(url)
{
  m_listener.support(methods::GET, std::bind(&Handler::handle_get, this, std::placeholders::_1));
  m_listener.support(methods::PUT, std::bind(&Handler::handle_put, this, std::placeholders::_1));
  m_listener.support(methods::POST, std::bind(&Handler::handle_post, this, std::placeholders::_1));
  m_listener.support(methods::DEL, std::bind(&Handler::handle_delete, this, std::placeholders::_1));
  m_listener.support(methods::OPTIONS, std::bind(&Handler::handle_options, this, std::placeholders::_1));
}

Handler::~Handler()
{

}

void Handler::handle_error(pplx::task<void>& t)
{
    try
    {
        t.get();
    }
    catch(...)
    {
        // Ignore the error, Log it if a logger is available
    }
}

//
// Get Request 
//
void Handler::handle_get(http_request message)
{
  ucout <<  message.to_string() << endl;
    
  QString resourcePath(":/html/index.html");

  QFile file(resourcePath);
  if (!file.open(QIODevice::ReadOnly))
  {
    message.reply(status_codes::InternalError,U("INTERNAL ERROR!"));
    return;
  }

  QTextStream in(&file);
  QString htmlFile = in.readAll();
  std::string html = htmlFile.toStdString();
  
  utility::string_t str = utility::conversions::to_string_t(html);
  message.reply(status_codes::OK, str, U("text/html"));

  return;
}

//
// A POST request
//
void Handler::handle_post(http_request message)
{
  ucout <<  message.to_string() << endl;

  json::value jsonResponse;
  
  message
    .extract_json()
    .then([&jsonResponse](pplx::task<json::value> task)
  {
    jsonResponse = task.get();
  }).wait();
  
  Person* ptr = new Person();
  deserialize(jsonResponse, ptr);
  print(ptr);
  
  delete ptr;

  http_response response(status_codes::OK);
  response.headers().add(U("Access-Control-Allow-Origin"), U("application/json"));
  response.set_body(jsonResponse);
  message.reply(response);
  return;
}

//
// A DELETE request
//
void Handler::handle_delete(http_request message)
{
  ucout <<  message.to_string() << endl;

  utility::string_t rep = U("WRITE YOUR OWN DELETE OPERATION");
  message.reply(status_codes::OK,rep);
  return;
}

//
// A PUT request 
//
void Handler::handle_put(http_request message)
{
  ucout <<  message.to_string() << endl;
  utility::string_t rep = U("WRITE YOUR OWN PUT OPERATION");
  message.reply(status_codes::OK,rep);
  return;
}

void Handler::handle_options(http_request request)
{
  // Allow CORS: Access-Control-Allow-Origin
  // This way it becomes possible to receive a request to consume json
  http_response response(status_codes::OK);
  response.headers().add(U("Allow"), U("GET, POST, OPTIONS"));
  response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
  response.headers().add(U("Access-Control-Allow-Methods"), U("GET, POST, OPTIONS"));
  response.headers().add(U("Access-Control-Allow-Headers"), U("Content-Type"));
  request.reply(response);
}

