﻿#include "Composite.h"

Composite::Composite():
  m_ptrPerson(0)
{
  m_ptrPerson = new Person();
  m_ptrPerson->setName("Frank");
}

Composite::~Composite()
{
  delete m_ptrPerson;
}

Person* Composite::get_m_ptrPerson()
{
  return m_ptrPerson;
}

void Composite::setPerson(Person* person)
{
  m_ptrPerson = person;
}
