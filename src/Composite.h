﻿#ifndef COMPOSITE_H
#define COMPOSITE_H

// cmake reflection
#include "common.h"
#include "Person.h"

ATTRIBUTES(INLINE)
class Composite
{
  FRIENDS_OF_TYPE(Composite); // Important: friend declarations of the generated functions (serialize and deserialize)

public:

  Composite();

  virtual ~Composite();

  Person* get_m_ptrPerson();

  void setPerson(Person* person);
  
private:
  
  FIELD Person* m_ptrPerson;
};

#endif // COMPOSITE_H

#include <gen/Composite.h.inl>
