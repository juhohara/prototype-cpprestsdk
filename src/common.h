﻿#pragma once

#include <cpprest/json.h>

#include <any>
#include <map>
#include <string>
#include <iostream>

using namespace web;
using namespace utility;
using namespace std; // I know it's bad...

#define FRIENDS_OF_TYPE(type)                                                                      \
    friend void print(const type& in);                                                             \
    friend json::value serialize(const type& src);                                                 \
    friend void deserialize(const json::value& src, type& dst);                                    \
    friend void print(type* in);                                                                   \
    friend json::value serialize(type* src);                                                       \
    friend void deserialize(const json::value& src, type* dst)


// helpers that don't expand to anything - used by the type parser
#define FIELD    // indicates the start of a field definition inside of a type
#define INLINE   // class attribute - emitted functions should be marked as inline
#define CALLBACK // field attribute - call the callback after the field changes
#define ATTRIBUTES(...) // comma-separated list of attributes and tags into this

namespace tag {
    struct special {};
}

inline void print(int in) { cout << "  [print: int]    " << in << endl; }

inline void print(double in) { cout << "  [print: double]  " << in << endl; }

inline void print(const string& in) { cout << "  [print: string] " << in << endl; }

inline void print(const string& in, tag::special) { cout << "  [print: string - overload] " << in << endl; }

inline json::value serialize(int in) { return in; }

inline json::value serialize(double in) { return in; }

inline json::value serialize(string in) 
{
  utility::string_t str = utility::conversions::to_string_t(in);
  return json::value::string(str); 
}

inline void deserialize(const json::value& src, int& dst) { dst = src.as_integer(); }

inline void deserialize(const json::value& src, double& dst) { dst = src.as_double(); }

inline void deserialize(const json::value& src, string& dst) { dst = utility::conversions::to_utf8string(src.as_string()); }
