﻿#ifndef PERSON_H
#define PERSON_H

// cmake reflection
#include "common.h"

#include <string>

ATTRIBUTES(INLINE)
class Person
{
  FRIENDS_OF_TYPE(Person); // Important: friend declarations of the generated functions (serialize and deserialize)

  public:

    Person();
    
    virtual ~Person();

    std::string& get_m_name();

    void setName(std::string name);

  private:

    FIELD std::string m_name;
};

#endif // HANDLER_H

#include <gen/Person.h.inl>
