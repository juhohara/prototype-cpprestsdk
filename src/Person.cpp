﻿#include "Person.h"

Person::Person():
 m_name("Unknown")
{
    
}

Person::~Person()
{

}

std::string& Person::get_m_name()
{
  return m_name;
}

void Person::setName(std::string name)
{
  m_name = name;
}
